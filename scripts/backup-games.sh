#!/usr/bin/env bash

drive=$1
mnt=$2
gog=$3
steam=$4
epic=$5

mkdir -p "$mnt"
sudo mount /dev/"$drive" "$mnt"

backup () {
	for dir in "$mnt"/"$1"/*/; do
		echo $dir
		s=$(du -s -B 100M "$dir" | cut -f 1)
		# s == 1 if size < 100M, a check for uninstalled games
		if [ "$s" -eq 1 ]; then
			echo skipping uninstalled game directory
		else
			# --delete option is used to remove redundant files after game updates
			rsync --archive --info=progress2 --partial --human-readable --delete "${dir%*/}" datum:data/games-bkp/windows/$2/
		fi
	done
}

backup "$gog" "gog_galaxy"
backup "$steam" "steam/steamapps/common"
backup "$epic" "epic_games"
