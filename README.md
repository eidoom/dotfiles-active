# [dotfiles-active](https://gitlab.com/eidoom/dotfiles-active)

## public

### bash

- [dotfiles-bash](https://gitlab.com/eidoom/dotfiles-bash)

### tmux

- [dotfiles-tmux-2](https://gitlab.com/eidoom/dotfiles-tmux-2)
- [dotfiles-tmux-remote](https://gitlab.com/eidoom/dotfiles-tmux-remote)

### (n)vim

- [dotfiles-nvim-lua](https://gitlab.com/eidoom/dotfiles-nvim-lua)
- [dotfiles-vim](https://gitlab.com/eidoom/dotfiles-vim)

### mpv

- [dotfiles-mpv](https://gitlab.com/eidoom/dotfiles-mpv)

### local

- [dotfiles-xps](https://gitlab.com/eidoom/dotfiles-xps)

### graveyard

- [dotfiles-alacritty](https://gitlab.com/eidoom/dotfiles-alacritty)
- [dotfiles-nvim-2](https://gitlab.com/eidoom/dotfiles-nvim-2)
- [dotfiles-nvim](https://gitlab.com/eidoom/dotfiles-nvim)
- [dotfiles-public](https://gitlab.com/eidoom/dotfiles-public)
- [dotfiles-system-backup](https://gitlab.com/eidoom/dotfiles-system-backup)
- [dotfiles-tmux-headless-light](https://gitlab.com/eidoom/dotfiles-tmux-headless-light)
- [dotfiles-tmux-remote-legacy](https://gitlab.com/eidoom/dotfiles-tmux-remote-legacy)
- [dotfiles-tmux](https://gitlab.com/eidoom/dotfiles-tmux)
- [dotfiles-torino](https://gitlab.com/eidoom/dotfiles-torino)
- [dotfiles-zsh](https://gitlab.com/eidoom/dotfiles-zsh)
- [home-server-aliases](https://gitlab.com/eidoom/home-server-aliases)
- [linux-on-windows-scripts](https://github.com/eidoom/linux-on-windows-scripts)
- [windows-scripts](https://github.com/eidoom/windows-scripts)

## private

### ssh

- [dotfiles-ssh](https://gitlab.com/eidoom/dotfiles-ssh)

### graveyard

- [dotfiles-home-desktop](https://gitlab.com/eidoom/dotfiles-home-desktop)
- [dotfiles-home-server](https://gitlab.com/eidoom/dotfiles-home-server)
- [dotfiles-ippp](https://gitlab.com/eidoom/dotfiles-ippp)
- [dotfiles-work-laptop-xps](https://gitlab.com/eidoom/dotfiles-work-laptop-xps)
- [dotfiles-work-laptop](https://gitlab.com/eidoom/dotfiles-work-laptop)
